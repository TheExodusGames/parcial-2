﻿using System;

namespace Examen_ejercicio_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int m1;
            string nombre;
            Empleados _Empleados = new Empleados("", "", "", "", "");
            Salarios _Salarios = new Salarios("", "", "", "", "");

            Console.WriteLine("Bienvenido al programa que");
            Console.WriteLine("---------------------------------------------------------");
            Console.WriteLine("Cual es el nombre del primer empleado: ");
            _Empleados.Empleado1 = Console.ReadLine();
            Console.WriteLine("Digite el salario: ");
            _Salarios.Salario1 = int.Parse(Console.ReadLine());
            Console.WriteLine("---------------------------------------------------------");
            Console.WriteLine("Cual es el nombre del segundo empleado: ");
            _Empleados.Empleado2 = Console.ReadLine();
            Console.WriteLine("Digite el salario: ");
            _Salarios.Salario2 = int.Parse(Console.ReadLine());
            Console.WriteLine("---------------------------------------------------------");
            Console.WriteLine("Cual es el nombre del tercer empleado: ");
            _Empleados.Empleado3 = Console.ReadLine();
            Console.WriteLine("Digite el salario: ");
            _Salarios.Salario3 = int.Parse(Console.ReadLine());
            Console.WriteLine("---------------------------------------------------------");
            Console.WriteLine("Cual es el nombre del cuarto empleado: ");
            _Empleados.Empleado4 = Console.ReadLine();
            Console.WriteLine("Digite el salario: ");
            _Salarios.Salario4 = int.Parse(Console.ReadLine());
            Console.WriteLine("---------------------------------------------------------");
            Console.WriteLine("Cual es el nombre del quinto empleado: ");
            _Empleados.Empleado5 = Console.ReadLine();
            Console.WriteLine("Digite el salario: ");
            _Salarios.Salario5 = int.Parse(Console.ReadLine());
            Console.WriteLine("---------------------------------------------------------");
            
            if (_Salarios.Salario1 > _Salarios.Salario2){
                m1 = _Salarios.Salario1;
                nombre = _Empleados.Empleado1;
            }
            else
            {   
                m1 = _Salarios.Salario2;
                nombre = _Empleados.Empleado2;
            }
            if (m1 < _Salarios.Salario3){
                m1 = _Salarios.Salario3;
                nombre = _Empleados.Empleado3;
            }
            if (m1 < _Salarios.Salario4){
                m1 = _Salarios.Salario4;
                nombre = _Empleados.Empleado4;
            }
            if (m1 < _Salarios.Salario5){
                m1 = _Salarios.Salario5;
                nombre = _Empleados.Empleado5;
            }
            Console.WriteLine("El empleado con mayor salario es " + nombre);
            Console.WriteLine("Tiene un salario de: " + m1);
        }
    }

    class Empleados
    {
        private string empleado1;
        private string empleado2;
        private string empleado3;
        private string empleado4;
        private string empleado5;

        public string Empleado1
        {
            get { return empleado1; }
            set { empleado1 = value; }
        }

        public string Empleado2
        {
            get { return empleado2; }
            set { empleado2 = value; }
        }

        public string Empleado3
        {
            get { return empleado3; }
            set { empleado3 = value; }
        }

        public string Empleado4
        {
            get { return empleado4; }
            set { empleado4 = value; }
        }

        public string Empleado5
        {
            get { return empleado5; }
            set { empleado5 = value; }
        }
        public void mostrarEmpleados()
        {
            Console.WriteLine("Empleado 1: " + Empleado1);
            Console.WriteLine("Empleado 2: " + Empleado2);
            Console.WriteLine("Empleado 3: " + Empleado3);
            Console.WriteLine("Empleado 4: " + Empleado4);
            Console.WriteLine("Empleado 5: " + Empleado5);
        }
         public Empleados (string pEmpleado1, string pEmpleado2, string pEmpleado3, string pEmpleado4, string pEmpleado5)
        {
            Empleado1 = pEmpleado1;
            Empleado2 = pEmpleado2;
            Empleado3 = pEmpleado3;
            Empleado4 = pEmpleado4;
            Empleado5 = pEmpleado5;
        }
    }

    class Salarios : Empleados
    {
        public Salarios (string pEmpleado1, string pEmpleado2, string pEmpleado3, string pEmpleado4, string pEmpleado5) 
        : base(pEmpleado1, pEmpleado2, pEmpleado3, pEmpleado4, pEmpleado5)
        {
        }
        private int salario1; 
        private int salario2;
        private int salario3;
        private int salario4;
        private int salario5;

        public int Salario1
        {
            get { return salario1; }
            set { salario1 = value; }
        }
        public int Salario2
        {
            get { return salario2; }
            set { salario2 = value; }
        }
        public int Salario3
        {
            get { return salario3; }
            set { salario3 = value; }
        }
        public int Salario4
        {
            get { return salario4; }
            set { salario4 = value; }
        }
        public int Salario5
        {
            get { return salario5; }
            set { salario5 = value; }
        }

         public void mostrarSalarios()
        {
            Console.WriteLine("Salario del empleado 1: " + Salario1);
            Console.WriteLine("Salario del empleado 2: " + Salario2);
            Console.WriteLine("Salario del empleado 3: " + Salario3);
            Console.WriteLine("Salario del empleado 4: " + Salario4);
            Console.WriteLine("Salario del empleado 5: " + Salario5);
        }
    }
}

/* By: ALEXANDER CORONADO.